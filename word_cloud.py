# Start with loading all necessary libraries
from itertools import count
import numpy as np
import pandas as pd
from os import path
from PIL import Image
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator

import matplotlib.pyplot as plt

# Load in the dataframe
df = pd.read_csv("C:/Users/Hin/Desktop/JDE Project/Final Project/Dataset/olist_products_dataset.csv", index_col=0)
df2 = pd.read_csv("C:/Users/Hin/Desktop/JDE Project/Final Project/Dataset/olist_order_items_dataset.csv", index_col=0)
df3 = pd.read_csv("C:/Users/Hin/Desktop/JDE Project/Final Project/Dataset/product_category_name_translation.csv", index_col=0)
df4 = pd.read_csv("C:/Users/Hin/Desktop/JDE Project/Final Project/Dataset/olist_customers_dataset.csv", index_col=0)
df_comment = pd.read_csv("C:/Users/Hin/Desktop/JDE Project/Final Project/Dataset/olist_order_reviews_dataset2.csv", index_col=0)
#print(df_comment.isnull().sum())

df_drop = df_comment.dropna()
df_keywords = {"keyword" : [],"count" : []}

for keyword in df_drop["review_comment_message"]:
    for i in keyword.split():
        df_keywords["keyword"].append(i)
        df_keywords["count"].append(1)

new_df = pd.DataFrame(df_keywords)

#print(df["product_category_name"].value_counts())
count_value = new_df.groupby("keyword")["count"].agg(np.sum).sort_values(ascending=False)
#print(count_value)

inner_join = pd.merge(left=df, right=df2, on="product_id", how="inner")
inner_join2 = pd.merge(left=inner_join, right=df3, on="product_category_name", how="inner")
top_product = inner_join2.groupby("product_category_name_english")["price"].agg(np.sum).sort_values(ascending=False)
top_city = df4.groupby("customer_state")["customer_unique_id"].agg("count").sort_values(ascending=False)

#print(top_city.head(10).index)
# Start with one review:
#text = df["product_category_name"][0]
text = " ".join(review for review in top_product.keys().astype(str))
text2 = " ".join(city.replace(" ","_") for city in top_city.head(100).index.astype(str))
text3 = " ".join(count_keyword for count_keyword in count_value.head(100).index.astype(str))

# Create and generate a word cloud image:
rose_mask = np.array(Image.open("C:/Users/Hin/Desktop/JDE Project/Final Project/Dataset/1599661.jpg"))
#print(rose_mask)

#wordcloud = WordCloud(background_color="white", width=800, height=400, mask=rose_mask, max_words=100, contour_width=2, contour_color='black').generate(text)
#wordcloud.to_file("C:/Users/Hin/Desktop/JDE Project/Final Project/Dataset/download.jpg")
wordcloud = WordCloud(background_color="white", width=800, height=400, max_font_size=5000).generate(text3)


# Display the generated image:
plt.imshow(wordcloud, interpolation='bilinear')
plt.axis("off")
plt.show()
